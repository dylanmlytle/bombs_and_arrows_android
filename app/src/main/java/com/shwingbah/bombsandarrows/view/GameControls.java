package com.shwingbah.bombsandarrows.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.shwingbah.bombsandarrows.R;
import com.shwingbah.bombsandarrows.controller.MainActivity;
import com.shwingbah.bombsandarrows.model.AppliedMove;
import com.shwingbah.bombsandarrows.model.JoinableGame;

import java.util.ArrayList;
import java.util.Set;

public class GameControls extends LinearLayout{

    public Set<JoinableGame> joinableGames;

    public TextView outputTextView;
    public EditText playerNameEditText;
    public EditText gameNameEditText;
    public Button createGameButton;
    public Spinner connectionSpinner;
    public Button joinGameButton;
    public Button submitMovesButton;

    public CreateGameInterface createGameInterface;
    public JoinGameInterface joinGameInterface;
    public SubmitMovesInterface submitMovesInterface;

    float buttonCornerRadiusWidthPercentage = 0.05f;
    float buttonBorderWidthPercentage = 0.05f;

    Context context;

    public GameControls(Context context) {
        super(context);

        this.context = context;

        setOrientation(VERTICAL);

        GradientDrawable buttonBackground = new GradientDrawable();
        buttonBackground.setColor(ContextCompat.getColor(context, R.color.defaultButtonBackgroundColor));
        buttonBackground.setCornerRadius(getWidth() * buttonCornerRadiusWidthPercentage);
        buttonBackground.setStroke((int)(getWidth() * buttonBorderWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));

        joinableGames = null;

        outputTextView = new TextView(context);

        playerNameEditText = new EditText(context);
        playerNameEditText.setText(context.getString(R.string.defaultPlayerName));

        gameNameEditText = new EditText(context);
        gameNameEditText.setText(context.getString(R.string.defaultGameName));

        createGameButton = new Button(context);

        createGameButton.setText(context.getString(R.string.createGameButtonText));
        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGameInterface.CreateGame();
            }
        });

        connectionSpinner = new Spinner(context);

        joinGameButton = new Button(context);
        joinGameButton.setText(context.getString(R.string.joinGameButtonText));
        joinGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connectionSpinner.getCount() > 0) {
                    joinGameInterface.JoinGame(((JoinableGame) connectionSpinner.getSelectedItem()).id);
                }
            }
        });

        submitMovesButton = new Button(context);

        submitMovesButton.setText(context.getString(R.string.submitMovesButtonText));
        submitMovesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitMovesInterface.SubmitMoves();
            }
        });

        addView(outputTextView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(playerNameEditText, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(gameNameEditText, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(createGameButton, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(connectionSpinner, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(joinGameButton, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(submitMovesButton, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
    }

    public void SetMessage(final String message) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                outputTextView.setText(message);
            }
        });
    }

    public void SetEnabledMoveSubmission(final boolean enabled) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                submitMovesButton.setEnabled(enabled);
            }
        });

    }

    public void SetEnabledStartOrJoinGame(final boolean enabled) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createGameButton.setEnabled(enabled);
                joinGameButton.setEnabled(enabled);
                playerNameEditText.setEnabled(enabled);
                gameNameEditText.setEnabled(enabled);
                connectionSpinner.setEnabled(enabled);
            }
        });
    }

    public interface CreateGameInterface {
        void CreateGame();
    }

    public interface JoinGameInterface {
        void JoinGame(String gameId);
    }

    public interface SubmitMovesInterface {
        void SubmitMoves();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) { // have to set relative background here because size width isn't known initially
        super.onSizeChanged(w, h, oldw, oldh);
        GradientDrawable buttonBackground = new GradientDrawable();
        buttonBackground.setColor(ContextCompat.getColor(context, R.color.defaultButtonBackgroundColor));
        buttonBackground.setCornerRadius(getWidth() * buttonCornerRadiusWidthPercentage);
        buttonBackground.setStroke((int)(getWidth() * buttonBorderWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));

        createGameButton.setBackground(buttonBackground);
        joinGameButton.setBackground(buttonBackground);
        submitMovesButton.setBackground(buttonBackground);
    }
}
