package com.shwingbah.bombsandarrows.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.shwingbah.bombsandarrows.R;
import com.shwingbah.bombsandarrows.model.AppliedMove;
import com.shwingbah.bombsandarrows.model.AvailableMove;

import static com.shwingbah.bombsandarrows.view.GameBoard.GetFlipped;

public class MoveTile extends LinearLayout{
    public AvailableMove availableMove;
    public AppliedMove appliedMove;
    public int index;

    public ShiftImageButton shiftLeftButton;
    public ShiftImageButton shiftRightButton;
    public ImageButton redButton;
    public ImageButton greenButton;
    public ImageButton blueButton;

    public OnClickListener shiftLeftOnClickListener;
    public OnClickListener shiftRightOnClickListener;

    float tileBorderThicknessWidthPercentage = 0.01f;
    float tileCornerRadiusWidthPercentage = 0.01f;

    public ImageView arrowImage;

    Context context;

    public MoveTile(Context context, AvailableMove availableMove, int index) {
        super(context);
        setOrientation(VERTICAL);

        this.context = context;

        setBackgroundResource(R.color.moveTileBackgroundColor);

        this.availableMove = availableMove;
        this.appliedMove = new AppliedMove();

        if(availableMove != null) {
            appliedMove.availablemoveid = availableMove.index;
        } else {
            appliedMove.availablemoveid = index;
        }
        this.index = index;
        appliedMove.index = index;
        this.appliedMove.bombcolor = "";

        LinearLayout colorButtons = new LinearLayout(context);

        redButton = new ImageButton(context);
        redButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                appliedMove.bombcolor = "red";
                UpdateUI();
            }
        });

        greenButton = new ImageButton(context);
        greenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                appliedMove.bombcolor = "green";
                UpdateUI();
            }
        });

        blueButton = new ImageButton(context);
        blueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                appliedMove.bombcolor = "blue";
                UpdateUI();
            }
        });

        colorButtons.addView(redButton, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        colorButtons.addView(greenButton, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        colorButtons.addView(blueButton, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));

        shiftLeftButton = new ShiftImageButton(context, this.index);
        shiftLeftButton.setImageResource(R.drawable.shift_left_arrow);
        shiftLeftButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        shiftLeftButton.setOnClickListener(shiftLeftOnClickListener);
        shiftRightButton = new ShiftImageButton(context, this.index);
        shiftRightButton.setImageResource(R.drawable.shift_right_arrow);
        shiftRightButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        shiftRightButton.setOnClickListener(shiftRightOnClickListener);

        arrowImage = new ImageView(context);
        arrowImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        UpdateUI();

        LinearLayout lowerMoveTile = new LinearLayout(context);
        lowerMoveTile.addView(shiftLeftButton, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        lowerMoveTile.addView(arrowImage, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 2));
        lowerMoveTile.addView(shiftRightButton, new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));

        addView(colorButtons, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        addView(lowerMoveTile, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 3));
    }

    public void UpdateUI()
    {
        boolean flipped = GetFlipped();
        if(availableMove != null && availableMove.move.equals("left")) {
            if(appliedMove.bombcolor.equals("red")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_red);
                } else {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_red);
                }
            } else if(appliedMove.bombcolor.equals("green")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_green);
                } else {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_green);
                }
            } else if(appliedMove.bombcolor.equals("blue")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_blue);
                } else {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_blue);
                }
            } else {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.right_arrow_image);
                } else {
                    arrowImage.setImageResource(R.drawable.left_arrow_image);
                }
            }
        } else if(availableMove != null && availableMove.move.equals("right")) {
            if(appliedMove.bombcolor.equals("red")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_red);
                } else {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_red);
                }
            } else if(appliedMove.bombcolor.equals("green")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_green);
                } else {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_green);
                }
            } else if(appliedMove.bombcolor.equals("blue")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.left_arrow_image_blue);
                } else {
                    arrowImage.setImageResource(R.drawable.right_arrow_image_blue);
                }
            } else {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.left_arrow_image);
                } else {
                    arrowImage.setImageResource(R.drawable.right_arrow_image);
                }
            }
        } else if(availableMove != null && availableMove.move.equals("forward")) {
            if(appliedMove.bombcolor.equals("red")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_red);
                } else {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_red);
                }
            } else if(appliedMove.bombcolor.equals("green")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_green);
                } else {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_green);
                }
            } else if(appliedMove.bombcolor.equals("blue")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_blue);
                } else {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_blue);
                }
            } else {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image);
                } else {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image);
                }
            }
        } else if(availableMove != null && availableMove.move.equals("backward")) {
            if(appliedMove.bombcolor.equals("red")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_red);
                } else {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_red);
                }
            } else if(appliedMove.bombcolor.equals("green")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_green);
                } else {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_green);
                }
            } else if(appliedMove.bombcolor.equals("blue")) {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image_blue);
                } else {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image_blue);
                }
            } else {
                if(flipped) {
                    arrowImage.setImageResource(R.drawable.forward_arrow_image);
                } else {
                    arrowImage.setImageResource(R.drawable.backward_arrow_image);
                }
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) { // have to set relative background here because size width isn't known initially
        super.onSizeChanged(w, h, oldw, oldh);
        GradientDrawable buttonBackground = new GradientDrawable();
        buttonBackground.setColor(ContextCompat.getColor(context, R.color.defaultButtonBackgroundColor));
        buttonBackground.setCornerRadius(getWidth() * tileCornerRadiusWidthPercentage);
        buttonBackground.setStroke((int)(getWidth() * tileBorderThicknessWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));

        shiftLeftButton.setBackground(buttonBackground);
        shiftRightButton.setBackground(buttonBackground);

        GradientDrawable redColorButtonBackground = new GradientDrawable();
        redColorButtonBackground.setColor(ContextCompat.getColor(context, R.color.moveTileRedSelectorColor));
        redColorButtonBackground.setCornerRadius(getWidth() * tileCornerRadiusWidthPercentage);
        redColorButtonBackground.setStroke((int)(getWidth() * tileBorderThicknessWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));
        redButton.setBackground(redColorButtonBackground);

        GradientDrawable greenColorButtonBackground = new GradientDrawable();
        greenColorButtonBackground.setColor(ContextCompat.getColor(context, R.color.moveTileGreenSelectorColor));
        greenColorButtonBackground.setCornerRadius(getWidth() * tileCornerRadiusWidthPercentage);
        greenColorButtonBackground.setStroke((int)(getWidth() * tileBorderThicknessWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));
        greenButton.setBackground(greenColorButtonBackground);

        GradientDrawable blueColorButtonBackground = new GradientDrawable();
        blueColorButtonBackground.setColor(ContextCompat.getColor(context, R.color.moveTileBlueSelectorColor));
        blueColorButtonBackground.setCornerRadius(getWidth() * tileCornerRadiusWidthPercentage);
        blueColorButtonBackground.setStroke((int)(getWidth() * tileBorderThicknessWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));
        blueButton.setBackground(blueColorButtonBackground);
    }

}
