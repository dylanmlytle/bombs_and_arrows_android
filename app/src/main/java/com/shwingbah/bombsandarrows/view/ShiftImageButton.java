package com.shwingbah.bombsandarrows.view;

import android.content.Context;
import android.widget.ImageButton;

/**
 * Created by dylan_lytle on 2/17/2017.
 */

public class ShiftImageButton extends ImageButton {
    int index;

    public ShiftImageButton(Context context, int index) {
        super(context);
        this.index = index;
    }
}
