package com.shwingbah.bombsandarrows.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.shwingbah.bombsandarrows.model.AppliedMove;
import com.shwingbah.bombsandarrows.model.AvailableMove;
import java.util.ArrayList;

public class MoveTiles extends LinearLayout {
    public ArrayList<MoveTile> moveTiles;

    public MoveTiles(Context context, ArrayList<AvailableMove> availableMoves) {
        super(context);
        moveTiles = new ArrayList<MoveTile>();
        if(availableMoves != null) {
            for (int i = 0; i < availableMoves.size(); i++) {
                MoveTile moveTile = new MoveTile(context, availableMoves.get(i), i);
                moveTiles.add(moveTile);
                moveTiles.get(i).shiftLeftButton.setOnClickListener(shiftLeftListener);
                moveTiles.get(i).shiftRightButton.setOnClickListener(shiftRightListener);
            }

            for (int i = 0; i < moveTiles.size(); i++) {
                Log.i("test","Add View");
                addView(moveTiles.get(i), new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
            }
        }
    }

    public void Update() {
        if(moveTiles == null) {
            return;
        }
        for(int i = 0; i < moveTiles.size(); i++) {
            moveTiles.get(i).UpdateUI();
        }
    }

    public ArrayList<AppliedMove> GetAppliedMoves() {
        ArrayList<AppliedMove> appliedMoves = new ArrayList<AppliedMove>();
        for(int i = 0; i < moveTiles.size(); i++)
        {
            appliedMoves.add(moveTiles.get(i).appliedMove);
        }
        return appliedMoves;
    }

    public android.view.View.OnClickListener shiftLeftListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = ((ShiftImageButton)v).index;

            String tempMove = moveTiles.get(index).availableMove.move;
            int tempIndex = moveTiles.get(index).availableMove.index;
            String tempColor = moveTiles.get(index).appliedMove.bombcolor;
            int tempAvailableMoveId = moveTiles.get(index).appliedMove.availablemoveid;

            int swapIndex = index - 1;
            if(swapIndex == -1) {
                swapIndex = moveTiles.size() - 1;
            }

            moveTiles.get(index).availableMove.move = moveTiles.get(swapIndex).availableMove.move;
            moveTiles.get(index).availableMove.index = moveTiles.get(swapIndex).availableMove.index;
            moveTiles.get(index).appliedMove.bombcolor = moveTiles.get(swapIndex).appliedMove.bombcolor;
            moveTiles.get(index).appliedMove.availablemoveid = moveTiles.get(swapIndex).appliedMove.availablemoveid;
            moveTiles.get(index).UpdateUI();

            moveTiles.get(swapIndex).availableMove.move = tempMove;
            moveTiles.get(swapIndex).availableMove.index = tempIndex;
            moveTiles.get(swapIndex).appliedMove.bombcolor = tempColor;
            moveTiles.get(swapIndex).appliedMove.availablemoveid = tempAvailableMoveId;
            moveTiles.get(swapIndex).UpdateUI();
        }
    };

    public android.view.View.OnClickListener shiftRightListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = ((ShiftImageButton)v).index;

            String tempMove = moveTiles.get(index).availableMove.move;
            int tempIndex = moveTiles.get(index).availableMove.index;
            String tempColor = moveTiles.get(index).appliedMove.bombcolor;
            int tempAvailableMoveId = moveTiles.get(index).appliedMove.availablemoveid;

            int swapIndex = index + 1;
            if(swapIndex == moveTiles.size()) {
                swapIndex = 0;
            }

            moveTiles.get(index).availableMove.move = moveTiles.get(swapIndex).availableMove.move;
            moveTiles.get(index).availableMove.index = moveTiles.get(swapIndex).availableMove.index;
            moveTiles.get(index).appliedMove.bombcolor = moveTiles.get(swapIndex).appliedMove.bombcolor;
            moveTiles.get(index).appliedMove.availablemoveid = moveTiles.get(swapIndex).appliedMove.availablemoveid;
            moveTiles.get(index).UpdateUI();

            moveTiles.get(swapIndex).availableMove.move = tempMove;
            moveTiles.get(swapIndex).availableMove.index = tempIndex;
            moveTiles.get(swapIndex).appliedMove.bombcolor = tempColor;
            moveTiles.get(swapIndex).appliedMove.availablemoveid = tempAvailableMoveId;
            moveTiles.get(swapIndex).UpdateUI();
        }
    };


}
