package com.shwingbah.bombsandarrows.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.shwingbah.bombsandarrows.R;
import com.shwingbah.bombsandarrows.model.Game;

public class GameBoard extends LinearLayout {
    public Game game;
    public static boolean flipBoard;
    public ImageView[][] boardTiles;
    public LinearLayout[] columnLayouts;

    Context context;

    int boardWidth = 9;
    int boardHeight = 7;
    float buttonCornerRadiusWidthPercentage = 0.05f;
    float buttonBorderWidthPercentage = 0.05f;

    public GameBoard(Context context, Game game)
    {
        super(context);

        this.game = game;
        this.context = context;

        flipBoard = false;

        boardTiles = new ImageView[boardWidth][boardHeight];
        columnLayouts = new LinearLayout[boardWidth];

        for(int i = 0; i < boardWidth; i++) {
            columnLayouts[i] = new LinearLayout(context);
            columnLayouts[i].setOrientation(VERTICAL);
            for(int j = 0; j < boardHeight; j++) {
                boardTiles[i][j] = new ImageView(context);
                columnLayouts[i].addView(boardTiles[i][j], new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            }
            addView(columnLayouts[i], new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        }

        Update();
    }

    public void SetBoardTileImageResource(final int x, final int y, final int resourceId) {

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boardTiles[x][y].setImageResource(resourceId);
            }
        });
    }

    public void Update()
    {
       if(game == null || game.bombs == null || game.bombs.size() < 3) {
            for(int i = 0; i < boardWidth; i++) {
                for (int j = 0; j < boardHeight; j++) {
                    SetBoardTileImageResource(i, j, android.R.color.transparent);
                }
            }
            return;
        }

        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                if(game.tiles.get(i).get(j).tiletype.equals("bush")) {
                    SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - i : i, flipBoard ? (boardHeight - 1) - j : j, R.drawable.bush_image);
                }
                else {
                    SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - i : i, flipBoard ? (boardHeight - 1) - j : j, android.R.color.transparent);
                }
            }
        }

        for(int i = 0; i < game.bombs.size(); i++)
        {
            if(game.bombs.get(i).color.equals("red")) {
                SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - game.bombs.get(i).coordinate.x : game.bombs.get(i).coordinate.x, flipBoard ? (boardHeight - 1) - game.bombs.get(i).coordinate.y : game.bombs.get(i).coordinate.y, R.drawable.red_bomb_image);
            } else if(game.bombs.get(i).color.equals("green")) {
                SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - game.bombs.get(i).coordinate.x : game.bombs.get(i).coordinate.x, flipBoard ? (boardHeight - 1) - game.bombs.get(i).coordinate.y : game.bombs.get(i).coordinate.y, R.drawable.green_bomb_image);
            } else if(game.bombs.get(i).color.equals("blue")) {
                SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - game.bombs.get(i).coordinate.x : game.bombs.get(i).coordinate.x, flipBoard ? (boardHeight - 1) - game.bombs.get(i).coordinate.y : game.bombs.get(i).coordinate.y, R.drawable.blue_bomb_image);
            }
        }

        for(int i = 0; i < game.players.size(); i++)
        {
            if(game.players.get(i).tower.isdestroyed) {
                SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - game.players.get(i).tower.coordinate.x : game.players.get(i).tower.coordinate.x, flipBoard ? (boardHeight - 1) - game.players.get(i).tower.coordinate.y : game.players.get(i).tower.coordinate.y, R.drawable.tower_image_red);
            } else {
                SetBoardTileImageResource(flipBoard ? (boardWidth - 1) - game.players.get(i).tower.coordinate.x : game.players.get(i).tower.coordinate.x, flipBoard ? (boardHeight - 1) - game.players.get(i).tower.coordinate.y : game.players.get(i).tower.coordinate.y, R.drawable.tower_image);
            }
        }
    }

    static public boolean GetFlipped() {
        return flipBoard;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) { // have to set relative background here because size width isn't known initially
        super.onSizeChanged(w, h, oldw, oldh);
        Log.i("test", "onSizeChanged");
        GradientDrawable tileBackground = new GradientDrawable();
        tileBackground.setColor(ContextCompat.getColor(context, R.color.defaultButtonBackgroundColor));
        tileBackground.setCornerRadius(getWidth() / 9 * buttonCornerRadiusWidthPercentage);
        tileBackground.setStroke((int)(getWidth() / 9 * buttonBorderWidthPercentage), ContextCompat.getColor(context, R.color.defaultButtonBorderColor));

        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                boardTiles[i][j].setBackground(tileBackground);
            }
        }
    }
}
