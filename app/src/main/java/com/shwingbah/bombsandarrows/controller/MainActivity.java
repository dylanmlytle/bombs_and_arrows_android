package com.shwingbah.bombsandarrows.controller;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.shwingbah.bombsandarrows.R;
import com.shwingbah.bombsandarrows.model.AppliedMove;
import com.shwingbah.bombsandarrows.model.AvailableMove;
import com.shwingbah.bombsandarrows.model.Game;
import com.shwingbah.bombsandarrows.model.JoinableGame;
import com.shwingbah.bombsandarrows.model.Player;
import com.shwingbah.bombsandarrows.server.GameManager;
import com.shwingbah.bombsandarrows.view.GameControls;
import com.shwingbah.bombsandarrows.view.GameBoard;
import com.shwingbah.bombsandarrows.view.MoveTiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {
    Game game;
    Player currentPlayer;

    GameBoard gameBoard;
    MoveTiles moveTiles;
    GameControls gameControls;

    boolean pollServer = false;
    int pollTime_milliseconds = 2000;

    int gameBoardWeight = 3;
    int moveTilesWeight = 1;

    int gameBoardAndMovesWeight = 3;
    int gameControlsWeight = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout rootLayout = new LinearLayout(this);

        ArrayList<AvailableMove> testMoves = new ArrayList<>();
        AvailableMove move1 = new AvailableMove();
        move1.index = 0;
        move1.move = "";
        AvailableMove move2 = new AvailableMove();
        move2.index = 0;
        move2.move = "";
        AvailableMove move3 = new AvailableMove();
        move3.index = 0;
        move3.move = "";
        testMoves.add(move1);
        testMoves.add(move2);
        testMoves.add(move3);

        gameBoard = new GameBoard(this, null);
        moveTiles = new MoveTiles(this, testMoves);
        gameControls = new GameControls(this);
        gameControls.joinGameInterface = new GameControls.JoinGameInterface() {
            @Override
            public void JoinGame(String gameId) {
                MainActivity.this.JoinGame();
            }
        };

        gameControls.createGameInterface = new GameControls.CreateGameInterface() {
            @Override
            public void CreateGame() {
                MainActivity.this.CreateGame();
            }
        };
        gameControls.submitMovesInterface = new GameControls.SubmitMovesInterface() {
            @Override
            public void SubmitMoves() {
                MainActivity.this.SubmitMoves();
            }
        };

        LinearLayout boardAndMoves = new LinearLayout(this);
        boardAndMoves.setOrientation(LinearLayout.VERTICAL);
        boardAndMoves.addView(gameBoard, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, gameBoardWeight));
        boardAndMoves.addView(moveTiles, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, moveTilesWeight));

        rootLayout.addView(boardAndMoves, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, gameBoardAndMovesWeight));
        rootLayout.addView(gameControls, new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, gameControlsWeight));

        setContentView(rootLayout);

        StartServerPolling();

        PopulateAvailableGameList();

        ProcessGameState();

        pollServer = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        UpdateUI();
    }

    public void UpdateUI()
    {
        gameBoard.Update();
        moveTiles.Update();
    }

    public void Refresh()
    {
        if(game == null)
        {
            PopulateAvailableGameList();
        }
        else {
            GetGame();
        }
    }

    public void PopulateAvailableGameList()
    {
        GameManager.GetInstance().GetJoinableGameList(this, new GameManager.JoinableGameListListener() {
            @Override
            public void JoinableGameListReceived(boolean succeeded, Set<JoinableGame> joinableGameList) {
                if(succeeded == true && joinableGameList != null) {
                    if(joinableGameList == gameControls.joinableGames) {
                        return;
                    }
                    List<JoinableGame> games = new ArrayList<JoinableGame>(joinableGameList);
                    ArrayAdapter<JoinableGame> dataAdapter = new ArrayAdapter<JoinableGame>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, games);
                    gameControls.connectionSpinner.setAdapter(dataAdapter);
                    gameControls.joinableGames = joinableGameList;
                }
            }
        });
    }

    public void CreateGame()
    {
        GameManager.GetInstance().CreateGame(this, gameControls.gameNameEditText.getText().toString(), gameControls.playerNameEditText.getText().toString(), new GameManager.CreateGameListener() {
            @Override
            public void CreateGameReceivedResponse(boolean succeeded, Game receivedGame) {
                currentPlayer = receivedGame.players.get(0);
                game = receivedGame;
                gameBoard.game = receivedGame;
                gameBoard.flipBoard = false;
                UpdateUI();
                ProcessGameState();
            }
        });
    }

    public void JoinGame()
    {
        GameManager.GetInstance().joinGame(this, gameControls.playerNameEditText.getText().toString(), ((JoinableGame)gameControls.connectionSpinner.getSelectedItem()).id, new GameManager.JoinGameListener() {
            @Override
            public void JoinGameReceivedResponse(boolean succeeded, Game receivedGame) {
                currentPlayer = receivedGame.players.get(1);
                game = receivedGame;
                gameBoard.game = receivedGame;
                gameBoard.flipBoard = true;
                ProcessGameState();
                UpdateUI();
            }
        });
    }

    public void SubmitMoves()
    {
        GameManager.GetInstance().SubmitMoves(this, currentPlayer.id, game.id, MainActivity.this.moveTiles.GetAppliedMoves(), new GameManager.SubmitMovesListener() {
            @Override
            public void SubmitMovesReceivedResponse(boolean succeeded, Game receivedGame) {
                game = receivedGame;
                gameBoard.game = receivedGame;
                ProcessGameState();
                UpdateUI();

            }
        });
    }

    public void GetGame()
    {
        GameManager.GetInstance().GetGame(this, game.id, currentPlayer.id, new GameManager.GetGameInformationListener() {
            @Override
            public void GameReceived(boolean succeeded, Game receivedGame) {
                if(game.state.equals(receivedGame.state) && game.players.get(0).isturn == receivedGame.players.get(0).isturn && game.players.get(1).isturn == receivedGame.players.get(1).isturn) {
                    return;
                }
                game = receivedGame;
                gameBoard.game = receivedGame;
                UpdateUI();
                ProcessGameState();
            }
        });
    }

    public void ProcessGameState()
    {
        if(game == null)
        {
            gameControls.SetMessage(getString(R.string.no_game_message));
            gameControls.SetEnabledMoveSubmission(false);
            gameControls.SetEnabledStartOrJoinGame(true);
            moveTiles.setEnabled(false);
            pollServer = true;
        }
        else
        {
            Log.i("test", "Game Not Null");
            gameControls.SetEnabledStartOrJoinGame(false);
            if(game.state.equals(getString(R.string.waitingforplayerstojoin_state))) {
                gameControls.SetMessage(getString(R.string.waitingforplayerstojoin_message));
                pollServer = true;
            } else if(game.state.equals(getString(R.string.turns_state))) {
                Log.i("test", "turns state");
                if(!game.players.get(currentPlayer.index).isturn) {
                    Log.i("test", "Their Turn");
                    for(int i = 0; i < game.players.size(); i++) {
                        if(game.players.get(i).isturn) {
                            gameControls.SetMessage(game.players.get(i).name + "'s Turn");
                        }
                    }
                    gameControls.SetEnabledMoveSubmission(false);
                    moveTiles.setEnabled(false);
                    pollServer = true;
                } else {
                    Log.i("test", "Your turn");
                    gameControls.SetMessage("Your Turn\nSelect Moves and Submit");
                    gameControls.SetEnabledMoveSubmission(true);
                    for(int i = 0; i < game.players.get(currentPlayer.index).availablemoves.size(); i++) {
                        Log.i("test", Integer.toString(game.players.get(currentPlayer.index).availablemoves.get(i).index) + " - " + game.players.get(currentPlayer.index).availablemoves.get(i).move);
                    }

                    moveTiles.setEnabled(true);
                    pollServer = false;
                }

                moveTiles.moveTiles.get(0).availableMove =  game.players.get(currentPlayer.index).availablemoves.get(0);
                moveTiles.moveTiles.get(0).appliedMove.availablemoveid = game.players.get(currentPlayer.index).availablemoves.get(0).index;
                moveTiles.moveTiles.get(1).availableMove =  game.players.get(currentPlayer.index).availablemoves.get(1);
                moveTiles.moveTiles.get(1).appliedMove.availablemoveid = game.players.get(currentPlayer.index).availablemoves.get(1).index;
                moveTiles.moveTiles.get(2).availableMove =  game.players.get(currentPlayer.index).availablemoves.get(2);
                moveTiles.moveTiles.get(2).appliedMove.availablemoveid = game.players.get(currentPlayer.index).availablemoves.get(2).index;
                moveTiles.Update();
            } else if(game.state.equals(getString(R.string.gamefinished_state))) {
                for(int i = 0; i < game.players.size(); i++) {
                    if(game.players.get(i).iswinner) {
                        gameControls.SetMessage("Game Over\n" + game.players.get(i).name + " Wins!!");
                    }
                }
                pollServer = false;
            }
        }
    }

    public void StartServerPolling() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask pollServerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        if(pollServer) {
                            Refresh();
                        }
                    }
                });
            }
        };
        timer.schedule(pollServerTask, 0, pollTime_milliseconds);
    }
}
