package com.shwingbah.bombsandarrows.server;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.shwingbah.bombsandarrows.R;
import com.shwingbah.bombsandarrows.model.AppliedMove;
import com.shwingbah.bombsandarrows.model.Game;
import com.shwingbah.bombsandarrows.model.JoinableGame;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class GameManager {
    private static GameManager gameManager = null;

    public static GameManager GetInstance() {
        if(gameManager == null) {
            gameManager = new GameManager();
        }
        return gameManager;
    }

    ///// Get Joinable Game List /////
    public interface JoinableGameListListener {
        void JoinableGameListReceived(boolean succeeded, Set<JoinableGame> joinableGameList);
    }
    public void GetJoinableGameList(Context context, final JoinableGameListListener listener) {
        URL joinableGameListURL = null;
        try {
            joinableGameListURL = new URL(GetServerBaseUrl(context) + "games");
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
            listener.JoinableGameListReceived(false, null);
        }

        AsyncTask<URL, Double, Set<JoinableGame>> joinableGameListRequestTask = new AsyncTask<URL, Double, Set<JoinableGame>>() {
            @Override
            protected Set<JoinableGame> doInBackground(URL... params) {
                String joinableGameListString = null;
                try {
                    joinableGameListString = GetDataFromURL(params[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.JoinableGameListReceived(false, null);
                }

                Gson gson = new Gson();
                JoinableGame[] joinableGames = gson.fromJson(joinableGameListString, JoinableGame[].class);
                return joinableGames == null ? null : new HashSet<JoinableGame>(Arrays.asList(joinableGames));
            }

            @Override
            protected void onPostExecute(Set<JoinableGame> availableGames) {
                super.onPostExecute(availableGames);
                listener.JoinableGameListReceived(true, availableGames);
            }
        };

        joinableGameListRequestTask.execute(joinableGameListURL);
    }

    ///// Create Game/////
    public interface CreateGameListener {
        void CreateGameReceivedResponse(boolean succeeded, Game receivedGame);
    }
    public void CreateGame(Context context,final String gameName, final String playerName, final CreateGameListener listener) {
        URL createGameURL = null;
        try {
            createGameURL = new URL(GetServerBaseUrl(context) + "games");
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
            listener.CreateGameReceivedResponse(false, null);
        }

        AsyncTask<URL, Double, Game> createGameRequestTask = new AsyncTask<URL, Double, Game>() {
            @Override
            protected Game doInBackground(URL... params) {
                String createGameResponseString = null;
                try {
                    JSONObject gameJsonObject = new JSONObject();
                    gameJsonObject.put("name", gameName);
                    JSONArray playersJsonArray = new JSONArray();
                    JSONObject playerJsonObject = new JSONObject();
                    playerJsonObject.put("name", playerName);
                    playersJsonArray.put(playerJsonObject);
                    gameJsonObject.put("players", playersJsonArray);
                    Log.i("test", "json - " + gameJsonObject.toString());

                    createGameResponseString = PostDataToURL(params[0], gameJsonObject.toString());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    listener.CreateGameReceivedResponse(false, null);
                }

                Log.i("test", "Received String - " + createGameResponseString);

                Gson gson = new Gson();
                Game createdGame = gson.fromJson(createGameResponseString, Game.class);

                return createdGame;
            }

            @Override
            protected void onPostExecute(Game createdGameResponse) {
                super.onPostExecute(createdGameResponse);
                listener.CreateGameReceivedResponse(true, createdGameResponse);
            }
        };

        createGameRequestTask.execute(createGameURL);
    }

    ///// Join Game /////
    public interface JoinGameListener {
        void JoinGameReceivedResponse(boolean succeeded, Game receivedGame);
    }
    public void joinGame(Context context, final String playerName, final String gameId, final JoinGameListener listener) {
        URL joinGameURL = null;
        try {
            joinGameURL = new URL(GetServerBaseUrl(context) + "games/" + gameId + "/join");
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
            listener.JoinGameReceivedResponse(false, null);
        }

        AsyncTask<URL, Double, Game> createGameRequestTask = new AsyncTask<URL, Double, Game>() {
            @Override
            protected Game doInBackground(URL... params) {
                String joinedGameString = null;
                try {
                    JSONObject playerJsonObject = new JSONObject();
                    playerJsonObject.put("name", playerName);
                    joinedGameString = PostDataToURL(params[0], playerJsonObject.toString());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    listener.JoinGameReceivedResponse(false, null);
                }

                Gson gson = new Gson();
                Log.i("test", joinedGameString);
                Game joinedGame = gson.fromJson(joinedGameString, Game.class);

                return joinedGame;
            }

            @Override
            protected void onPostExecute(Game createdGameResponse) {
                super.onPostExecute(createdGameResponse);
                listener.JoinGameReceivedResponse(true, createdGameResponse);
            }
        };

        createGameRequestTask.execute(joinGameURL);
    }

    // Submit Moves /////
    public interface SubmitMovesListener {
        void SubmitMovesReceivedResponse(boolean succeeded, Game receivedGame);
    }
    public void SubmitMoves(Context context, final String playerId, final String gameId, final ArrayList<AppliedMove> moves, final SubmitMovesListener listener) {
        URL submitMovesURL = null;
        try {
            submitMovesURL = new URL(GetServerBaseUrl(context) + "games/" + gameId + "/submitmoves/" + playerId);
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
            listener.SubmitMovesReceivedResponse(false, null);
        }

        AsyncTask<URL, Double, Game> createGameRequestTask = new AsyncTask<URL, Double, Game>() {
            @Override
            protected Game doInBackground(URL... params) {
                String submitMovesResponseString = null;

                try {
                    JSONArray movesJSON = new JSONArray();
                    for(int i = 0; i < moves.size(); i++)
                    {
                        JSONObject move = new JSONObject();
                        move.put("index", moves.get(i).index);
                        move.put("bombcolor", moves.get(i).bombcolor);
                        move.put("availablemoveid", moves.get(i).availablemoveid);
                        movesJSON.put(move);
                    }
                    submitMovesResponseString = PostDataToURL(params[0], movesJSON.toString());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    listener.SubmitMovesReceivedResponse(false, null);
                }

                Gson gson = new Gson();
                Log.i("test", submitMovesResponseString);
                Game createdGame = gson.fromJson(submitMovesResponseString, Game.class);

                return createdGame;
            }

            @Override
            protected void onPostExecute(Game createdGameResponse) {
                super.onPostExecute(createdGameResponse);
                listener.SubmitMovesReceivedResponse(true, createdGameResponse);
            }
        };

        createGameRequestTask.execute(submitMovesURL);
    }

    ///// Get Game /////
    public interface GetGameInformationListener {
        void GameReceived(boolean succeeded, Game receivedGame);
    }
    public void GetGame(Context context, final String gameID, final String playerID, final GetGameInformationListener listener) {
        URL getGameURL = null;
        try {
            getGameURL = new URL(GetServerBaseUrl(context) + "games/" + gameID + "/" + playerID);
        }
        catch(MalformedURLException e) {
            e.printStackTrace();
            listener.GameReceived(false, null);
        }

        AsyncTask<URL, Double, Game> getGameRequestTask = new AsyncTask<URL, Double, Game>() {
            @Override
            protected Game doInBackground(URL... params) {
                String getGameString = null;
                try {
                    getGameString = GetDataFromURL(params[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.GameReceived(false, null);
                }

                Gson gson = new Gson();
                Game game = gson.fromJson(getGameString, Game.class);

                return game;
            }

            @Override
            protected void onPostExecute(Game game) {
                super.onPostExecute(game);
                listener.GameReceived(true, game);
            }
        };

        getGameRequestTask.execute(getGameURL);
    }

    ///// Local Functions /////

    private String GetServerBaseUrl(Context context) {
        return context.getString(R.string.server_base_url);
    }

    private String GetDataFromURL(URL url) throws IOException, NullPointerException
    {
        String returnDataString = null;
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        InputStream inputStream = connection.getInputStream();
        Scanner scanner = new Scanner(inputStream);
        StringBuilder stringBuilder = new StringBuilder();
        while(scanner.hasNext()) {
            stringBuilder.append(scanner.nextLine());
        }
        returnDataString = stringBuilder.toString();
        inputStream.close();

        return returnDataString;
    }

    private String PostDataToURL(URL url, String jsonString) throws IOException
    {
        String returnDataString = null;
        HttpURLConnection connection = null;
        connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.connect();

        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes(jsonString);
        outputStream.flush();
        outputStream.close();

        InputStream inputStream = connection.getInputStream();
        Scanner scanner = new Scanner(inputStream);
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNext()) {
            stringBuilder.append(scanner.nextLine());
        }
        returnDataString = stringBuilder.toString();
        inputStream.close();

        return returnDataString;
    }
}
