package com.shwingbah.bombsandarrows.model;

import java.util.ArrayList;

public class Player {
    public int index;
    public String id;
    public String name;
    public ArrayList<AvailableMove> availablemoves;
    public ArrayList<AppliedMove> appliedmoves;
    public Tower tower;
    public boolean isturn;
    public boolean iswinner;
}
