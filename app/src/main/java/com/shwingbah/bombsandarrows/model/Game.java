package com.shwingbah.bombsandarrows.model;

import java.util.ArrayList;

public class Game
{
    public String name;
    public String id;
    public String state;
    public ArrayList<Player> players = new ArrayList<Player>();
    public ArrayList<ArrayList<Tile>> tiles = new ArrayList<ArrayList<Tile>>();
    public ArrayList<Bomb> bombs = new ArrayList<Bomb>();
}
